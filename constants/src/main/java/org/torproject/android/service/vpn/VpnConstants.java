package org.torproject.android.service.vpn;

public interface VpnConstants {

    int FILE_WRITE_BUFFER_SIZE = 2048;

    String SHELL_CMD_PS = "toolbox ps";

    int TOR_DNS_PORT_DEFAULT = 5400;

    String LOCAL_ACTION_PORTS = "ports";

    String EXTRA_DNS_PORT = "org.torproject.android.intent.extra.DNS_PORT";

    String EXTRA_TRANS_PORT = "org.torproject.android.intent.extra.TRANS_PORT";
}
