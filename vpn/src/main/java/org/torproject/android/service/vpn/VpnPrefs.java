package org.torproject.android.service.vpn;

public interface VpnPrefs {

    String PREFS_DNS_PORT = "PREFS_DNS_PORT";

    String PREFS_KEY_TORIFIED = "PrefTord";

    String SOCKS_PROXY_PORT = "VPN_SOCKS_PROXY_PORT";

}
